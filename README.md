# configs

A repository for all of my dotfiles and builds.

## dwm laptop

This is the dwm configutation of my laptop

![dwm laptop](https://i.imgur.com/6MtL6rc.png)

## dwm bar laptop

The bar script for my dwm build

![dwm bar laptop](http://i.imgur.com/UjvE3h3.png)
