#!/bin/sh

mpd &
picom --config .config/compton/compton.conf &
setbg config load &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
nm-applet &
redshift &
clipit -n &
pcloud &
deadd-notification-center &
xset +fp /home/adi/.local/share/fonts
xset fp rehash &
bash ~/.dwm/statusbar.sh &
setxkbmap hu &
