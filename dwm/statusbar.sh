#!/bin/bash

fok="+0.0°C"
updates="0"
school="Vakációig: 0"

while true; do echo "🖥  $(sensors | grep Package | awk '{print $4}')" > ~/scripts/statusbar-temp; sleep 30; done &
while true; do echo "$(checkupdates | wc -l)" > ~/scripts/statusbar-updates; sleep 1800 & echo $! > ~/.dwm/updatepid && wait $!; done &
while true; do echo "☀  $(( $(echo "$(( $(echo "$(( $(echo "$(( $(date +"%s" -d "2021-06-15") - $(date +"%s") ))") / 60 ))") / 60 ))") / 24))" > ~/scripts/statusbar-school; sleep 6000; done &

while true
do

	fok="$(cat ~/scripts/statusbar-temp)"
	updates="$(cat ~/scripts/statusbar-updates)"
	school="$(cat ~/scripts/statusbar-school)"

	should_display_mpd="$(cat ~/.dwm/bar-isMPDDisplayed)"
	if [ "$should_display_mpd" = "true" ]; then
		mpd=" ▶  $(mpc current)  | "
		mpd_paused="$(mpc status | grep paused)"
		[ ! -z "$mpd_paused" ] && mpd=" ⏹  $(mpc current)  | "
	else
		mpd=""
	fi

	if [ "$(pulsemixer --get-mute)" = "0" ]
	then
		xsetroot -name "$mpd 🔉  $(pulsemixer --get-volume | cut -d' ' -f1)%  |  $(netmodule)  |  $(micmute)  |  📦  $updates  |  $fok  |  $school  |  $(date +"%Y.%m.%d (%A)  %T") "
	else
		xsetroot -name "$mpd 🔇  $(pulsemixer --get-volume | cut -d' ' -f1)%  |  $(netmodule)  |  $(micmute)  |  📦  $updates  |  $fok  |  $school  |  $(date +"%Y.%m.%d (%A)  %T") "
	fi
	sleep 1 & echo $! > ~/.dwm/sleeppid && wait $!
done
