/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating = 1;
static const unsigned int gappx = 12;
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Cantarell:size=10", "Noto Color Emoji:pixelsize=12" };
static const char dmenufont[]       = "Cantarell:size=10";
static const char col_gray1[]       = "#2a2a2a";
static const char col_gray2[]       = "#464646";
static const char col_gray3[]       = "#c5c5c5";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#00667e";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",      NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "Firefox",   NULL,     NULL,           0,         0,          0,          -1,        -1 },
	{ "st",        NULL,     NULL,           0,         0,          1,          -1,        -1 },
	{ "Alacritty", NULL,     NULL,           0,         0,          1,          -1,        -1 },
	{ "trayer",    NULL,     NULL,           0,         1,          0,           1,        -1 },
	{ NULL,        NULL,     "Event Tester", 0,         1,          0,           1,        -1 }, /* xev */
	{ NULL,        NULL,     "dragon",       0,         1,          0,           1,        -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *term2cmd[]  = { "alacritty", NULL };
static const char *notifcentercmd[]  = { "notiifcationcenter", NULL };

/* Program launchers */
static const char *firefoxcmd[] = { "firefox", NULL };
static const char *offcmd[] = { "off", NULL };
static const char *chromiumcmd[] = { "chromium", NULL };
static const char *lockscreencmd[] = { "lock", NULL };
static const char *pcmancmd[] = { "pcmanfm", NULL };
static const char *screenshootercmd[] = { "screenshot", NULL };
static const char *recordcmd[] = { "record", NULL };
static const char *surfcmd[] = { "tabbed", "surf", "-pe", NULL };
static const char *ripcordcmd[] = { "ripcord", NULL };
static const char *rangercmd[] = { "alacritty", "-e", "ranger", NULL };

/* Wallpapers */
static const char *bgresetcmd[] = { "setbg", "config", "load", NULL };
static const char *bgfallbackcmd[] = { "setbg", "config", "restore", NULL };

/* Media */
static const char *volupcmd[] = { "volumectl", "+", NULL };
static const char *voldowncmd[] = { "volumectl", "-", NULL };
static const char *mutesoundcmd[] = { "volumectl", "mute", NULL };
static const char *micmutecmd[] = { "volumectl", "micmute", NULL };

/* MPD */
static const char *mpd_togglecmd[] = { "mpc", "toggle", NULL };
static const char *mpd_nextcmd[] = { "mpc", "next", NULL };
static const char *mpd_prevcmd[] = { "mpc", "prev", NULL };
static const char *mpd_barcmd[] = { "toggle_bar_audiodisplay", NULL };

/* USB mounting */
static const char *usbmountcmd[] = { "usbmount", NULL };
static const char *usbunmountcmd[] = { "usbunmount", NULL };
static const char *usbautomountcmd[] = { "toggle_usb_check", NULL };

/* Other */
static const char *updatecmd[] = { "alacritty", "-e", "autoupdate", NULL };
static const char *emojicmd[] = { "emoji-picker", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_t,      spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_t,      spawn,          {.v = term2cmd } },
	{ MODKEY,                       XK_a,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_g,      killclient,     {0} },
	{ MODKEY,                       XK_n,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ControlMask,           XK_f,      togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,						XK_b,      spawn,          {.v = firefoxcmd } },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = offcmd } },
	{ MODKEY|ShiftMask,	            XK_b,      spawn,          {.v = chromiumcmd } },
	{ MODKEY|ShiftMask,             XK_l,      spawn,          {.v = lockscreencmd } },
	{ MODKEY|ShiftMask,             XK_f,      spawn,          {.v = pcmancmd } },
	{ MODKEY,                       XK_s,      spawn,          {.v = screenshootercmd } },
	{ MODKEY|ControlMask,           XK_r,      spawn,          {.v = recordcmd } },
	{ MODKEY,                       XK_F2,     spawn,          {.v = voldowncmd } },
	{ MODKEY,                       XK_F3,     spawn,          {.v = volupcmd } },
	{ MODKEY,                       XK_F4,     spawn,          {.v = mutesoundcmd } },
	{ MODKEY|ShiftMask,             XK_m,      spawn,          {.v = micmutecmd } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = mpd_togglecmd } },
	{ MODKEY,                       XK_y,      spawn,          {.v = mpd_prevcmd } },
	{ MODKEY,                       XK_c,      spawn,          {.v = mpd_nextcmd } },
	{ MODKEY|ShiftMask,             XK_a,      spawn,          {.v = mpd_barcmd } },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          {.v = notifcentercmd } },
	{ MODKEY,                       XK_w,      spawn,          {.v = bgfallbackcmd } },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = bgresetcmd } },
	{ MODKEY,                       XK_u,      spawn,          {.v = usbmountcmd } },
	{ MODKEY|ShiftMask,             XK_u,      spawn,          {.v = usbunmountcmd } },
	{ MODKEY|ControlMask,           XK_u,      spawn,          {.v = usbautomountcmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = emojicmd } },
	{ MODKEY|ControlMask,			XK_b,      spawn,          {.v = surfcmd } },
	{ MODKEY|ShiftMask, 			XK_d,      spawn,          {.v = ripcordcmd } },
	{ MODKEY,           			XK_r,      spawn,          {.v = rangercmd } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(		                XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkStatusText,        MODKEY,         Button2,        spawn,          {.v = updatecmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

