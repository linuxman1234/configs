#!/bin/sh

time(){
	time_format="%Y.%m.%d (%A)  %T"
	output="$(date +"$time_format")"
	echo "$output"
}

volume(){
	muted="no"
	[ "$(pulsemixer --get-mute)" = "1" ] && muted="yes"
	
	if [ "$muted" = "no" ]; then
		output="Hangerő: $(pulsemixer --get-volume | cut -d' ' -f1)"'%'
	else
		output="néma ($(pulsemixer --get-volume | cut -d' ' -f1)"'%'")"
	fi
	echo "$output"
}

battery(){
	status="$(cat /sys/class/power_supply/BAT0/status)"
	capacity="$(cat /sys/class/power_supply/BAT0/capacity)"
	format="Akkumulátor: $capacity"'%'
	[ $capacity -lt 20 ] && format="Akkumulátor: !$capacity"'%'
	[ "$status" = "Discharging" ] && [ $capacity -gt 20 ] && format="Akkumulátor: $capacity"'%'" (merül)"
	[ "$status" = "Discharging" ] && [ $capacity -lt 20 ] && format="Akkumulátor: !$capacity"'%'" (merül)"

	[ "$status" = "Charging" ] && [ $capacity -gt 20 ] && format="Akkumulátor: $capacity"'%'" (töltődik)"
	[ "$status" = "Charging" ] && [ $capacity -lt 20 ] && format="Akkumulátor: !$capacity"'%'" (töltődik)"

	echo "$format"
}

while true; do
	format="  $(battery)  |  $(volume)  |  $(time)  "
	xsetroot -name "$format"
	sleep 1 & echo $! > sleeppid && wait
done
